import pickle
import os
from django.shortcuts import render
from mlthesis.settings import BASE_DIR
import sqlparse
import numpy as np
import math
from collections import Counter
import pandas as pd
from utils import simple_stats
# importanto de random forest
import sklearn.ensemble
from sklearn.externals import joblib
from  sklearn.model_selection import train_test_split
from sklearn.model_selection import cross_val_score
dfrf = pd.read_csv(os.path.join(BASE_DIR, 'api','random_fores_values.csv'),encoding='utf-8',sep='|',header=0)
df_stats = pd.read_csv(os.path.join(BASE_DIR, 'api','df_stasts.csv'),encoding='utf-8',sep='|',header=0,index_col=0)
X = dfrf.as_matrix(['longitud', 'entropia','legitimo_g','malo_g'])
Y = np.array(dfrf['tipo'].tolist())
# clf = sklearn.ensemble.RandomForestClassifier(n_estimators=20,criterion='entropy')
# X_train, X_test, y_train, y_test, index_train, index_test = train_test_split(X, Y, dfrf.index, test_size=0.2)
# clf.fit(X_train, y_train)

def parse_it(raw_sql):
    parsed = sqlparse.parse(str(raw_sql))
    return [token._get_repr_name() for parse in parsed for token in parse.tokens if token._get_repr_name() != 'Whitespace']

def n_gram(lst, N):
    ngrams = []
    for n in range(0,N):
        ngrams += zip(*(lst[i:] for i in range(n+1)))
    
    return [str(tuple) for tuple in ngrams]

def g_aggregate(sequence, name):
    try:
        g_scores = [df_stats.ix[item][name] for item in sequence]
    except KeyError:
        return 0
    return sum(g_scores)/len(g_scores) if g_scores else 0

def entropy(s):
    p = Counter(s)
    lns = len(s)
    suma = 0.0
    for count in p.values():
        suma = suma - (count/float(lns)*math.log(count/float(lns), 2))
    return suma
def token_expansion(series, types):
    _tokens, _types = zip(*[(token,token_type) for t_list,token_type in zip(series,types) for token in t_list])
    return pd.Series(_tokens), pd.Series(_types)

# Create your views here.
def index(request):
    res = ''
    # return render(request, 'home/index.html', {'menu':menu})
    search = request.POST.get('search')
    if search is None or search is '':
      print('no hacer nada')
      res = ''
    else:
      # testing
      parsed_sql = parse_it(search)
      print(parsed_sql)
      ngram_list = n_gram(parsed_sql, 3)
      print(ngram_list)
      malicious_g = g_aggregate(ngram_list, 'malo_g')
      print(malicious_g)
      legit_g = g_aggregate(ngram_list, 'legitimo_g')
      print(legit_g)
      _X = [len(parsed_sql), entropy(search), legit_g, malicious_g]
      clf = joblib.load(os.path.join('api','model_v1.pkl'))
      predictions = clf.predict([_X])
      print(predictions)
      res = str(predictions[0])  
    context ={'res':res}   
    return render(request, 'home/index.html',context)