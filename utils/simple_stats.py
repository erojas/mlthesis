
import pandas as pd
import numpy as np
import collections
import math
import heapq
import math

class FixedHeap(list):
    def __init__(self, N):
        self._N = N
        super(list, self).__init__()
    def push(self, item):
        if (len(self) == self._N):
            heapq.heappushpop(self,item)
        else:
            heapq.heappush(self, item)
    def sorted(self):
        return heapq.nlargest(self._N, self)
    def max(self):
        return heapq.nlargest(1, self)[0]

class GTest():
    def __init__(self):
        ''' Init for GTest '''

    def highest_gtest_scores(self, series_a, series_b, N=10, matches=10, reverse=False, min_volume=0):
        if N > 0:
            topN = series_a.value_counts().head(N).index.tolist()
            drop = []
            for i, row in series_a.iteritems():
                if row not in topN:
                    drop.append(i)

            series_a = series_a.drop(drop)
            series_b = series_b.drop(drop)
            series_a.index = range(len(series_a))
            series_b.index = range(len(series_b))

        mar_dist_a = series_a.value_counts().astype(float)  # Marginal distibution of A
        mar_dist_b = series_b.value_counts().astype(float)  # Marginal distibution of B
        mar_dist_a = mar_dist_a[mar_dist_a > min_volume]
        cont_table = collections.defaultdict(lambda : collections.Counter())
        for val_a, val_b in zip(series_a.values, series_b.values):
            cont_table[val_a][val_b] += 1

        
        valores = cont_table.values()
        keys_primary = cont_table.keys()
        valores1 = []
        for k, v in enumerate(valores):
            valores1.append(v)
            # pdb.set_trace()
            # print(k, v)
        
       
        dataframe = pd.DataFrame(valores1, index=keys_primary)
        dataframe.fillna(0, inplace=True)

        if matches > 0:
            (rows, cols) = dataframe.shape
            cols_to_keep = []
            for r in range(rows):
                cols_to_keep += dataframe.iloc[r].sort_values(ascending=False).head(matches).index.tolist()[1:]
            drop_cols = set(dataframe.columns.tolist()).difference(set(cols_to_keep))
            dataframe = dataframe.drop(drop_cols, 1)

        columns = dataframe.columns.tolist()
        dataframe_cd = pd.DataFrame.copy(dataframe)
        dataframe_cd['total'] = mar_dist_a
        for column in columns:
            dataframe_cd[column] = dataframe_cd[column] / dataframe_cd['total']

        dataframe_g = pd.DataFrame.copy(dataframe)
        dataframe_g = dataframe_g.merge(dataframe_cd.rename(columns=lambda x: x + '_cd'), left_index=True, right_index=True)
        for column in columns:
            dataframe_g[column+'_exp'] = mar_dist_a * mar_dist_b[column] / total_count
            dataframe_g[column+'_g'] = [self.g_test_score(count, exp) for count, exp in zip(dataframe_g[column], dataframe_g[column+'_exp'])]

        return dataframe, dataframe_cd, dataframe_g

    def g_test_score(self, count, expected):
        if (count == 0):
            return 0
        else:
            return 2.0 * count * math.log(count/expected)

def _test():
    
    import os
    import pprint
    import ast

    # Open a dataset (relative path)
    cwd = os.getcwd()
    file_path = os.path.join(cwd, 'test_data.csv')
    dataframe = pd.read_csv(file_path)
    dataframe.head()

    g_test = GTest()
    names, match_list, df = g_test.highest_gtest_scores(dataframe['name'], dataframe['status'], N=5)
    print ('\n<<< Names with highest correlation to status >>>')
    pprint.pprint(zip(names, match_list))
    print (df)

if __name__ == "__main__":
    _test()