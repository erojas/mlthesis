import numpy as np
from scipy.stats import chisqprob, chisquare

def gtest(f_obs, f_exp=None, ddof=0):
    f_obs = np.asarray(f_obs, 'f')
    k = f_obs.shape[0]
    f_exp = np.array([np.sum(f_obs, axis=0) / float(k)] * k, 'f') if f_exp is None else np.asarray(f_exp, 'f')
    g = 2 * np.add.reduce(f_obs * np.log(f_obs / f_exp))
    return g, chisqprob(g, k - 1 - ddof)

if __name__ == "__main__":
    print(gtest([9.0, 8.1, 2, 1, 0.1, 20.0], [10, 5.01, 6, 4, 2, 1]))
    print(gtest([1.01, 1.01, 4.01], [1.00, 1.00, 4.00]))
    print(gtest([2, 1, 6], [4, 3, 2]))
